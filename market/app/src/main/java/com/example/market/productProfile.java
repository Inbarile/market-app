package com.example.market;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.squareup.picasso.Picasso;

public class productProfile extends myBaseActivity implements View.OnClickListener {

    ImageView p_pic;
    TextView p_name, u_name, u_addr, u_email;
    Button returnScroll;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_profile);
        Bundle extras = getIntent().getExtras();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        p_pic = (ImageView)findViewById(R.id.p_img);
        p_name = (TextView)findViewById(R.id.p_name);
        u_name = (TextView)findViewById(R.id.u_name);
        u_addr = (TextView)findViewById(R.id.u_addr);
        u_email = (TextView)findViewById(R.id.u_email);
        returnScroll = (Button)findViewById(R.id.return_menu);
        returnScroll.setOnClickListener(this);
        Picasso.get().load(extras.getString("p_pict")).into(p_pic);

        u_name.setText("User: " + extras.getString("u_name"));
        p_name.setText(extras.getString("p_name"));
        u_addr.setText("Address: " + extras.getString("u_addr"));
        u_email.setText("Email: " +extras.getString("u_email"));
        p_pic.setScaleType(ImageView.ScaleType.FIT_XY);
    }

    @Override
    public void onClick(View v) {
        finish();


    }

}
