package com.example.market;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Menu extends myBaseActivity implements View.OnClickListener {
    Button GTproducts;
    Button myProfile;
    Button exit;
    Bundle extras;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        // Adding values

        GTproducts = (Button)findViewById(R.id.btn_products);
        GTproducts.setOnClickListener(this);

        myProfile = (Button)findViewById(R.id.btn_myProfile);
        myProfile.setOnClickListener(this);

        exit = (Button)findViewById(R.id.btn_signup);
        exit.setOnClickListener(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public void onClick(View v) {
        extras = getIntent().getExtras();
        if(v == GTproducts) {
            Intent intent = new Intent(this, ProductsViewer.class);
            intent.putExtra("usrUsername", extras.getString("usrUsername"));
            startActivity(intent);
        }

        if(v == myProfile)
        {
            Intent intent = new Intent(this,clientProfile.class);
            intent.putExtra("usrUsername", extras.getString("usrUsername"));
            intent.putExtra("usrEmail", extras.getString("usrEmail"));
            intent.putExtra("usrPassword", extras.getString("usrPassword"));
            startActivity(intent);
        }

        if(v == exit)
        {
            finish();
        }
    }


}