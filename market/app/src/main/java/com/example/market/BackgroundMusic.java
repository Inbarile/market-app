package com.example.market;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;

public class BackgroundMusic extends Service {
    private final IBinder binder = (IBinder) new SimpleBinder();
    private static final String TAG = null;
    MediaPlayer player;

    @Override
    public  void onCreate() {
        super.onCreate();
        player = MediaPlayer.create(this, R.raw.background);
        player.setLooping(true);
        player.setVolume(12,12);
    }

    public int onStartCommand(Intent intent, int flags, int startId){
        player.start();
        return 1;
    }

    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return binder;
    }
    public IBinder onUnBind(Intent arg0)
    {
        return null;
    }
    public void onStop()
    {
        player.stop();
    }
    public void onPause(){}

    @Override
    public void onDestroy(){
        player.stop();
        player.release();
    }

    @Override
    public void onLowMemory() {}

    public class SimpleBinder extends Binder{
        BackgroundMusic getService()
        {
            return BackgroundMusic.this;
        }
    }
}