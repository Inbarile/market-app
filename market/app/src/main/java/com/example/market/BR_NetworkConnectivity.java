package com.example.market;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class BR_NetworkConnectivity extends BroadcastReceiver {

    @Override
    // In the manifest there are the permissions needed
    // In case of this BR the needed permissions are ->wifi, ->network.
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        // Reaching to the System service.
        NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        // Getting the information needed.
        boolean isConnected = wifi != null && wifi.isConnectedOrConnecting() ||
                mobile != null && mobile.isConnectedOrConnecting();
        // Checking if the wifi or mobile is connected.
        if(isConnected){
            Toast.makeText(context, "Network connection was detected.", Toast.LENGTH_LONG).show();

        }
        else
        {
            Toast.makeText(context, "No Network connection was detected.", Toast.LENGTH_LONG).show();

        }
    }
}