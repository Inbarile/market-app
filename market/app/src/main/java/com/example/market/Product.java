package com.example.market;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;


public class Product {
    private String name;
    private String location;
    private String district;
    private String pict;
    private String id;

    public Product() {
    }

    public Product(String name, String location, String district, String id, String pic) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.district = district;
        this.pict = pic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPict() {
        return pict;
    }

    public void setPict(String pict) {
        this.pict = pict;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
