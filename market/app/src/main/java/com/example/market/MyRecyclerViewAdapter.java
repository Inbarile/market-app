package com.example.market;

import android.content.Context;
import android.graphics.pdf.PdfDocument;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder>{
    private ArrayList<Product> pData;
    private LayoutInflater mInflater;

    public MyRecyclerViewAdapter(Context context, ArrayList<Product> data) {
        this.mInflater = LayoutInflater.from(context);
        this.pData = data;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = mInflater.inflate(R.layout.recyclerview_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Product myP = pData.get(holder.getAdapterPosition());
        holder.textView.setText(myP.getName());
    }

    @Override
    public int getItemCount() {
        if (pData == null) { return 0; }
        return pData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textView;
        public LinearLayout linearLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.productName);
            linearLayout = (LinearLayout)itemView.findViewById(R.id.horizontal_row_layout);
        }
    }
}