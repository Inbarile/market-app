package com.example.market;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class myBaseActivity extends AppCompatActivity {
    protected android.view.Menu menu;// Global Menu Declaration
    static boolean soundCheck = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        this.menu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog.Builder builder;
        AlertDialog dialog;
        // The functions of the menu is getting set.
        switch (item.getItemId()) {
            case R.id.sound:
                menu.getItem(1).setVisible(true);
                menu.getItem(0).setVisible(false);
                stopService(new Intent(this, BackgroundMusic.class));
                soundCheck = false;
                return true;

            case R.id.nosound:
                menu.getItem(0).setVisible(true);
                menu.getItem(1).setVisible(false);
                startService(new Intent(this, BackgroundMusic.class));
                soundCheck = true;
                return true;
            case R.id.about_app:
                builder = new AlertDialog.Builder(this);
                builder.setTitle("About Market");
                builder.setIcon(R.mipmap.ic_launcher);
                builder.setMessage("Welcome to the Market!, I'm hoping it's not your first time.\n" +
                        "Market is all about giving to each other. You own items you don't need? You feel like making someone else happy?" +
                        "It's your place to shine! Upload your products you the application. Contract others about their own products." +
                        "And the most important thing is....." +
                        "To enjoy!");
                dialog = builder.create();
                dialog.setButton(dialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });

                dialog.show();
                return true;
            case R.id.profile:
                builder = new AlertDialog.Builder(this);
                builder.setTitle("About me");
                builder.setIcon(R.mipmap.ic_launcher);
                builder.setMessage("Hey!, I'm the developer of The Market app you are using.\n" +
                        "Some information about me: \n" +
                        "1) My name is Inbar.\n" +
                        "2) I developed this when I was 17 year old. \n" +
                        "3) Im from Israel.\n" +
                        "4) Im learning CS in high school and this is my final project.\n" +
                        "5) And the most important one, Im graduating from high school!\n" +
                        "I hoping you enjoy using my app. :)");
                dialog = builder.create();
                dialog.setButton(dialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });

                dialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
