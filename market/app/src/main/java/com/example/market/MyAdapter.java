package com.example.market;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

import static androidx.core.content.ContextCompat.startActivity;

public class MyAdapter extends ArrayAdapter<Product> {
    String image;
    String id;
    String usrId;
    String email;
    ArrayList<Product> productList = new ArrayList<>();
    ImageView imageView;
    FirebaseDatabase database;
    DatabaseReference myRef;
    Intent intent;
    Product p;
    public MyAdapter(Context context, int textViewResourceId, ArrayList<Product> objects) {
        super(context, textViewResourceId, objects);
        productList = objects;
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.products_list, null);
        // The inflater connects between the xml file, to the Context java
        // So in 53:54 the inflater connects the listView handled to the correct xml file.
        TextView textView = (TextView) v.findViewById(R.id.name);
        TextView textView2 = (TextView) v.findViewById(R.id.location);
        image = productList.get(position).getPict();
        imageView = (ImageView) v.findViewById(R.id.nike_shirt);

        // Getting the product info from the current position in the array.
        id = productList.get(position).getId();
        textView.setText(productList.get(position).getName());
        textView2.setText(productList.get(position).getLocation());

        // If the P has photo it will be uploaded by the picasso.
        if(productList.get(position).getPict().length() > 0)
        {
            Picasso.get().load(productList.get(position).getPict()).into(imageView);
        }
        p = productList.get(position);
        v.setTag(p); // Setting the tag in order to keep the current product to it's own onClick.
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Product position = (Product) v.getTag();
                intent = new Intent(getContext().getApplicationContext(), productProfile.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                uploaderId(position);
            }
        });
        return v;

    }
    void uploaderId(Product p)
    {
        id = p.getId();
        Query q = myRef.child("connectors").orderByValue();
        q.addValueEventListener(new ValueEventListener() {
            @Override
            // An event listener that gets the username of the product owner.
            // taking the product id, by that getting the username of the product holder
            public void onDataChange(DataSnapshot dataSnapshot) {
                long count = dataSnapshot.getChildrenCount();
                for(DataSnapshot item : dataSnapshot.getChildren())
                {
                    String newC = item.getKey().toString();
                    if(newC.equals(id))
                    {// Checking if the iterated item is the search item.
                        usrId = item.getValue().toString();
                        break;
                    }
                }
                // With the desired username, the user's info is taken
                Query q = myRef.child("users").orderByValue();
                q.addValueEventListener(new ValueEventListener() {
                    @Override
                    // An event listener that get the product uploader email using it's username.
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        long count = dataSnapshot.getChildrenCount();
                        for(DataSnapshot item : dataSnapshot.getChildren())
                        {
                            Subscriber newS = item.getValue(Subscriber.class);
                            if(newS.getUsername().equals(usrId))
                            {// If the item is not already taken by someone
                                email = newS.getEmail();
                                break;
                            }
                        }
                        // Posting all the details to the next activity.
                        intent.putExtra("p_name", p.getName());
                        intent.putExtra("p_pict", p.getPict());
                        intent.putExtra("u_addr", p.getLocation());
                        intent.putExtra("u_name", usrId);
                        intent.putExtra("u_email", email);
                        getContext().startActivity(intent);
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        throw databaseError.toException(); // Don't ignore errors
                    }
                });
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                throw databaseError.toException(); // Don't ignore errors
            }
        });
    }
}

