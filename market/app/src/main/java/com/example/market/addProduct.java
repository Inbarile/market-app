package com.example.market;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class addProduct extends myBaseActivity implements View.OnClickListener {

    Button done;
    Button upload;
    EditText title;
    EditText location;
    FirebaseDatabase database;
    DatabaseReference myRef;
    private ImageView imageView;
    // -------------
    Spinner spinner;
    String area = "";
    String pic_url = "";
    Product newP;
    String new_id = "";
    String itemName;
    String locationN;
    String uploader;
    //--------------
    //Firebase
    StorageReference storageReference;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addingproduct);

        Bundle extras = getIntent().getExtras();
        uploader = extras.getString("usrUsername");


        title = (EditText)findViewById(R.id.item_te);
        location = (EditText)findViewById(R.id.location_te);
        initSpinner();
        done = (Button)findViewById(R.id.done);
        done.setOnClickListener(this);
        upload = (Button)findViewById(R.id.upload_ph);
        upload.setOnClickListener(this);

        imageView = (ImageView) findViewById(R.id.my_avatar);
        storageReference = FirebaseStorage.getInstance().getReference();
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
        getNewId();


   }


    //Function: The function is responisble on setting the dialog in which the user choose which application to use in order to upload a picture
    private void selectImage(Context context) {
        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Choose your profile picture");
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                // Option of taking live photo from camera.
                if (options[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, 123);
                // Option of choosing photo from gallery
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto , 1);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {

            if (requestCode == 123 && resultCode == RESULT_OK ) {
                // Taking the data from the application
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                // Setting the taken photo in the imageView
                imageView.setImageBitmap(imageBitmap);

                // Comprassing the data to a byte array in order ro upload into the storage.
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] data2 = baos.toByteArray();

                // Uploading the photo to the storage,
                StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("images").child(new_id);
                UploadTask uploadTask = storageReference.putBytes(data2);
                uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>()
                {@Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                }
                });

            }
            else if (requestCode == 1 && resultCode == RESULT_OK && data != null) {
                if (resultCode == RESULT_OK) {
                    try {
                        // Taking the choosen photo from gallery and converting it to bitmap
                        final Uri imageUri = data.getData();
                        final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        // Setting the bitmap in the imageView
                        imageView.setImageBitmap(selectedImage);
                        // Comprassing the data to a byte array in order ro upload into the storage.
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        byte[] data2 = baos.toByteArray();
                        // Uploading the Picture to the firebase's storage.
                        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("images").child(new_id);
                        UploadTask uploadTask = storageReference.putBytes(data2);
                        uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>()
                        {@Override
                        public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                        }
                        });


                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        raiseToast("Something went wrong");
                    }

                } else {
                    raiseToast("You haven't picked Image");
                }
            }
        }
    }

    private void initSpinner()
    {
        spinner = (Spinner) findViewById(R.id.spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.areas_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }
    void raiseToast(String msg)
    {
        Toast toast = Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT);
        toast.setMargin(0,-200);
        toast.show();
    }


    void getNewId()
    {
        Query q = myRef.child("items").orderByValue();
        // The command of the listener executes onDataChange method immediately.
        // After it's done the listener stops listening
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                long count = snapshot.getChildrenCount();
                new_id = Integer.toString((int) count);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void add_product_to_db(String name, String location)
    {
        // Uploading the finished product into the firebase.
        area = spinner.getSelectedItem().toString();
        newP = new Product(name, location,area,new_id, pic_url);
        myRef.child("items").child(newP.getId()).setValue(newP);
    }

    public void upload()
    {
        // Taking the uploaded image's url in order to link the image to the firebase.
        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("images").child(new_id);
        storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                pic_url = uri.toString();
                add_product_to_db(itemName, locationN);
                // Connecting uploader and item via new table "connectors"
                myRef.child("connectors").child(new_id).setValue(uploader);
                //Getting back to the product viewer
                finish();
            }
        });
    }


    @Override
    public void onClick(View v) {
        if (v == done) {

            itemName = title.getText().toString();
            locationN = location.getText().toString();
            if (!(itemName.isEmpty() || itemName.isEmpty())) {
                // Using dialog to reassure upload of product.

                AlertDialog.Builder uploadDialog = new AlertDialog.Builder(this);
                uploadDialog.setTitle("Alert");
                uploadDialog.setMessage("Are you sure you want to upload the product?");
                AlertDialog dialog = uploadDialog.create();
                dialog.setButton(dialog.BUTTON_POSITIVE, "Yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                upload();
                            }
                        });
                dialog.setButton(dialog.BUTTON_NEGATIVE, "No",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                dialog.show();
            } else//Should raise a toast about invalid data.
            {
                raiseToast("Missing data, fill all fields.");
            }


        }
        else if(v== upload)
        {
            selectImage(addProduct.this);
        }
    }
}
