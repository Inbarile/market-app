package com.example.market;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;

public class ProductsViewer extends myBaseActivity implements View.OnClickListener {
    ListView listView;
    ArrayList<Product> productList=new ArrayList<>();
    Button all_fil;
    Button north_fil;
    Button south_fil;
    Button haifa_fil;
    Button jerus_fil;
    Button ta_fil;
    Button judah_fil;
    Button center_fil;
    Button return_menu;
    ImageButton addProduct;
    FirebaseDatabase database;
    DatabaseReference myRef;
    String userName;
    boolean flag = true;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products_viewer);
        Bundle extras = getIntent().getExtras();
        userName = extras.getString("usrUsername");
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
        initButtons();
        initSearchWidgets();
        if(flag = false)
        {
            productList.clear();
            flag = true;
        }
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        addProducts();

    }

    private void initButtons()
    {
        addProduct = (ImageButton)findViewById(R.id.add);
        addProduct.setOnClickListener(this);

        all_fil = (Button)findViewById(R.id.allFilter);
        all_fil.setOnClickListener(this);

        north_fil = (Button)findViewById(R.id.northernFilter);
        north_fil.setOnClickListener(this);

        south_fil = (Button)findViewById(R.id.southFilter);
        south_fil.setOnClickListener(this);

        ta_fil = (Button)findViewById(R.id.telAvivFilter);
        ta_fil.setOnClickListener(this);

        jerus_fil = (Button)findViewById(R.id.jerusalemFilter);
        jerus_fil.setOnClickListener(this);

        haifa_fil = (Button)findViewById(R.id.haifaFilter);
        haifa_fil.setOnClickListener(this);

        center_fil = (Button)findViewById(R.id.centralFilter);
        center_fil.setOnClickListener(this);
        listView = (ListView) findViewById(R.id.simpleListView);

        judah_fil = (Button)findViewById(R.id.judahFilter);
        judah_fil.setOnClickListener(this);

        return_menu = (Button)findViewById(R.id.return_menu);
        return_menu.setOnClickListener(this);

    }
    public void raiseToast(String msg)
    {
        Toast toast=Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT);
        toast.setMargin(0,-200);
        toast.show();
    }
    void  addProducts()
    {
        flag = false;
        Query q = myRef.child("items").orderByValue();
        q.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                long count = dataSnapshot.getChildrenCount();
                // Getting all the products in the firebase and setting them into the array List.
                for(DataSnapshot item : dataSnapshot.getChildren())
                {
                    Product newP = item.getValue(Product.class);
                    productList.add(newP);
                }

                MyAdapter myAdapter = new MyAdapter(getApplicationContext(), R.layout.products_list, productList);
                listView.setAdapter(myAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                raiseToast("nooo!!");
                throw databaseError.toException(); // Don't ignore errors
            }
        });

    }

    private void initSearchWidgets()
    {
        SearchView searchView = (SearchView) findViewById(R.id.productSearcher);

        // Using the search Line activity.
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String s)
            {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s)
            {
                ArrayList<Product> filteredProducts = new  ArrayList<Product>();

                for(Product  p : productList)
                {
                    if(p.getName().toLowerCase().contains(s.toLowerCase()))
                    {// Adding only the products searches by the user.
                        filteredProducts.add(p);
                    }
                }

                MyAdapter adapter = new MyAdapter(getApplicationContext(), R.layout.products_list, filteredProducts);
                listView.setAdapter(adapter);

                return false;
            }

        });
    }

    private void filteredList(String status) {
        ArrayList<Product> filteredProducts = new  ArrayList<Product>();
        if (status == "all")
        {
            filteredProducts = productList;
        }
        for(Product  p : productList)
        { // Taking the status given and filtering the products list by that.
            if(p.getDistrict().toLowerCase().contains(status.toLowerCase()))
            {
                filteredProducts.add(p);
            }
        }// Setting the adapter with the new list.
        MyAdapter adapter = new MyAdapter(getApplicationContext(), R.layout.products_list, filteredProducts);
        listView.setAdapter(adapter);
    }
    @Override
    public void onClick(View v) {
        Bundle extras = getIntent().getExtras();
        if(v==addProduct)
        {
            Intent productAddIntent = new Intent(this, addProduct.class);
            productAddIntent.putExtra("usrUsername", userName);
            this.productList.clear();
            startActivity(productAddIntent);
        }
        else if(v == return_menu)
        {
            finish();
        }
        else if(v == north_fil)
        {
            filteredList("northern");
        }
        else if(v == haifa_fil)
        {
            filteredList("haifa");
        }
        else if(v == jerus_fil)
        {
            filteredList("Jerusalem");
        }
        else if(v == south_fil)
        {
            filteredList("southern");
        }
        else if(v == ta_fil)
        {
            filteredList("Tel Aviv");
        }
        else if(v == center_fil)
        {
            filteredList("central");
        }
        else if(v == judah_fil)
        {
            filteredList("judah");
        }
        else
        {
            filteredList("all");
        }
    }
}
