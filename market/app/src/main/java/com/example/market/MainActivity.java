package com.example.market;

import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.core.ValueEventRegistration;

import java.time.chrono.MinguoChronology;
import java.util.ArrayList;

public class MainActivity extends myBaseActivity implements View.OnClickListener {
    BR_NetworkConnectivity reciver_nw = new BR_NetworkConnectivity(); // The Br is activated
    EditText pwd;
    EditText usr;
    EditText mail;
    Button login;
    Button signup;
    FirebaseDatabase database;
    DatabaseReference myRef;
    boolean flag = false;
    Subscriber u;
    Subscriber newUsr;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);  // Here the java file know what it's corresponding xml file.
        startService(new Intent(this, BackgroundMusic.class));

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
        pwd = (EditText)findViewById(R.id.password_te);
        usr = (EditText)findViewById(R.id.username_te);
        mail = (EditText)findViewById(R.id.email_t);
        login = (Button)findViewById(R.id.btn_login);
        signup = (Button)findViewById(R.id.btn_signup);
        login.setOnClickListener(this);
        signup.setOnClickListener(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    void subsribe_new_user(Subscriber usr)
    { // Putting the user details into the firebase.
        myRef.child("users").child(usr.username).setValue(usr);
    }

    void goToMenu()
    {
        // Sending the logged in user info to the next intent.
        Intent intent = new Intent(this, Menu.class);
        intent.putExtra("usrUsername", newUsr.getUsername());
        intent.putExtra("usrEmail", newUsr.getEmail());
        intent.putExtra("usrPassword", newUsr.getPassword());
        startActivity(intent);
    }

    public void raiseToast(String msg)
    {
        Toast toast=Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT);
        toast.setMargin(0,-200);
        toast.show();
    }
    @Override
    public void onClick(View v)
    {
        String username = usr.getText().toString();
        String password = pwd.getText().toString();
        newUsr = new Subscriber(username, password);
        Query q = myRef.child("users").orderByValue();

        if(v==login)
        {
            q.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    boolean isUserNameFound = false;
                    String msg = "";
                    for(DataSnapshot dst : snapshot.getChildren())
                    {
                        // Setting the iterated user info taken from fb.
                        u = new Subscriber(dst.getKey(), dst.child("password").getValue().toString(), dst.child("email").getValue().toString());
                        if(u.username.equals(newUsr.getUsername()))
                        {
                            isUserNameFound = true;
                            if(u.password.equals(password))
                            { // If the password match the user name
                                raiseToast("Logged in successfully");
                                goToMenu();
                            }
                            else
                            {
                                raiseToast("Password incorrect");

                            }

                        }
                    }
                    if(!isUserNameFound)
                    { // If no user name was found
                        raiseToast("Username does not exists");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });

        }
        if(v==signup)
        {
            if(!flag) {
                mail.setVisibility(View.VISIBLE);
                flag = true;
            }
            else {
                newUsr.setEmail(mail.getText().toString());

                q.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override

                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        boolean userAlreadyExists = false;
                        String msg2 = "";

                        for (DataSnapshot dst : snapshot.getChildren()) {
                            // Iterating for each user in order to check whether user is already signed in the app
                            u = new Subscriber(dst.getKey().toString(), dst.child("password").getValue().toString(), dst.child("email").getValue().toString());
                            if (u.username.equals(newUsr.getUsername())) {
                                raiseToast("Username is already taken");
                                userAlreadyExists = true;
                            }
                        }
                        if (!userAlreadyExists) {
                            // If user is indeed new to the app, subscribing it to the system.
                            subsribe_new_user(newUsr);
                            raiseToast("Signup was successful");
                            goToMenu();
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }

        }
    }


}