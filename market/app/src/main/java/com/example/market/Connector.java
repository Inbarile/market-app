package com.example.market;


public class Connector {

    protected String username;
    protected String id;

    public Connector(String usr,String id)
    {
        this.username = usr;
        this.id = id;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getID() {
        return id;
    }

    public void setID(String password) {
        this.id = password;
    }

}
