package com.example.market;

public class Subscriber {

    protected String username;
    protected String password;
    protected String email;

    public Subscriber() {
    }

    public Subscriber(String usr,String pwd,String mail)
    {
        this.email = mail;
        this.password = pwd;
        this.username = usr;
    }

    public Subscriber(String usr, String pwd)
    {
        this.username = usr;
        this.password = pwd;
        this.email= "inbar@gmail.com";
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
