package com.example.market;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ListView;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class clientProfile extends myBaseActivity implements View.OnClickListener {

    TextView greating;
    TextView name;
    TextView password;
    TextView mail;
    Button b_ret;
    RecyclerView mRecyclerView;
    ArrayList<Product> my_Products = new ArrayList<Product>();
    LinearLayoutManager layoutManager;
    MyRecyclerViewAdapter adapter;
    FirebaseDatabase database;
    DatabaseReference myRef;
    ArrayList<String> myPid;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Bundle extras = getIntent().getExtras();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();

        greating = (TextView)findViewById(R.id.m_greating);
        greating.setText("Hello "+ extras.getString("usrUsername"));

        name = (TextView)findViewById(R.id.m_username);
        name.setText(extras.getString("usrUsername"));

        password = (TextView)findViewById(R.id.m_password);
        password.setText(extras.getString("usrPassword"));

        mail = (TextView)findViewById(R.id.m_email);
        mail.setText(extras.getString("usrEmail"));

        b_ret = (Button)findViewById(R.id.returnM);
        b_ret.setOnClickListener(this);

        myP();


    }

    void myP()
    {
        myPid = new ArrayList<>();
        String userName = name.getText().toString();
        Query q = myRef.child("connectors").orderByValue();
        q.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                long count = dataSnapshot.getChildrenCount();
                for(DataSnapshot item : dataSnapshot.getChildren())
                {
                    String newC = item.getValue().toString();
                    if(newC.equals(userName))
                    {// If the item is not already taken by someone
                        myPid.add(item.getKey());
                    }
                }
                getMyProducts();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                throw databaseError.toException(); // Don't ignore errors
            }
        });
    }

    void  getMyProducts()
    {
        Query q = myRef.child("items").orderByValue();
        q.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                long count = dataSnapshot.getChildrenCount();
                for(DataSnapshot item : dataSnapshot.getChildren())
                {
                    Product newP = item.getValue(Product.class);
                    if(myPid.contains(newP.getId()))
                    {// If the item is not already taken by someone
                        my_Products.add(newP);
                    }
                }

                // Setting recycler view
                layoutManager= new LinearLayoutManager(getApplicationContext() ,LinearLayoutManager.HORIZONTAL, false);
                mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
                adapter = new MyRecyclerViewAdapter(clientProfile.this, my_Products);
                mRecyclerView.setHasFixedSize(true);
                mRecyclerView.setLayoutManager(layoutManager);
                mRecyclerView.setAdapter(adapter);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                throw databaseError.toException(); // Don't ignore errors
            }
        });

    }



    @Override
    public void onClick(View v) {

        if (v == b_ret)
        {
            finish();
        }
    }
}
